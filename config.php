<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'owNLDStV&s<SBk-;$<_sS4C%POMEPCJ4&hek3-tt(S2k`Z#jO }+o79eF=9VX91+');
define('SECURE_AUTH_KEY',  'g{#xz,XmT:2Gvs;#K^ksl]`iWOfz[B4q<%6+xO&DI-QKu6WC$+z|.LY|Q^>p5TN-');
define('LOGGED_IN_KEY',    'qBw6;>Gs+?_/gpr%:4)l3oaMfH*+hFR$]j#8cAs.PL(S|R@LtJ5c)f8c~GTQ=o@g');
define('NONCE_KEY',        '!$!9-S-km~5r6oBwqD{+sFtbZ7_+&7D)O{R#8091F2j`4_=U|t9dv10xSp,N1%2|');
define('AUTH_SALT',        '~r$IX%2R>%/| )|ZG?5^=( J8fReo=L3qeU-u{E@/nA6X~=bYzUokS`(o3ReKd&w');
define('SECURE_AUTH_SALT', '$K(,l9cNNB;7Q`MY`,q<Q@T|jRNDuZakYtJ>JXN^+N:6g)NZ}AqcRB:DvryXuJL-');
define('LOGGED_IN_SALT',   'L?:s|<jlWcYpt01[-`PEAadI*Naaf/P_xd@d7yriqm$~ljU}bQR;~u.(}!#MEW!i');
define('NONCE_SALT',       '}|g<ZC^vA]{a2_3rcx*#F<v9s8NseOz-8;_M]$1fSyM %0c)PL|ly=_`y#QKHt/{');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
        define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');


